package by.kolovaitis.acq

import android.content.Context

object ProjectPreferences {
    private const val SHARED_PREFERENCES_NAME = "StorageToken"
    private const val TOKEN_KEY = "token"
    fun getToken(context: Context):String?{
        val preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        return preferences.getString(TOKEN_KEY, null)
    }
    fun setToken(context: Context, token:String?){
        val preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(TOKEN_KEY, token)
        editor.apply()
    }

}