package by.kolovaitis.acq

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.location.LocationServices
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.yandex.mapkit.MapKitFactory
import java.io.InputStream


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private val viewModel: MyViewModel by viewModels()

    private lateinit var appBarConfiguration: AppBarConfiguration

    private val LOCATION_REQUEST_CODE = 1
    private val PICK_IMAGE = 1

    var fab: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (ProjectPreferences.getToken(this) == null) {
            val loginIntent = Intent(this, LoginActivity::class.java)
            startActivity(loginIntent)
            finish()
        } else {
            setContentView(R.layout.activity_main)
            val toolbar: Toolbar = findViewById(R.id.toolbar)
            setSupportActionBar(toolbar)

            fab = findViewById(R.id.fab)

            val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
            val navView: NavigationView = findViewById(R.id.nav_view)
            val navController = findNavController(R.id.nav_host_fragment)

            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.nav_map, R.id.nav_messages
                ), drawerLayout
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)

            refreshMyLocation()
            val header = navView.getHeaderView(0)
            val imageView = header.findViewById<ImageView>(R.id.imageViewMyProfile)
            imageView.setOnClickListener {
                pickImage()
            }
            viewModel.me.observe(this, Observer {
                if (it.image != null) {
                    imageView.setImageBitmap(it.image)
                } else {
                    imageView.setImageResource(R.drawable.ic_twotone_face_24)
                }
                val textView = header.findViewById<TextView>(R.id.textViewMyName)
                if (it.name != null) {
                    textView.text = "${it.name} @${it.login}"
                }
            })
            navView.setNavigationItemSelectedListener(this)

            MapKitFactory.setApiKey(getString(R.string.yandex_api_key))
            MapKitFactory.initialize(this)


        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun refreshMyLocation() {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                LOCATION_REQUEST_CODE
            )
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    viewModel.myLocation = Location(location.latitude, location.longitude)
                    viewModel.loadUsersForMe()
                }
            }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        refreshMyLocation()
    }

    fun pickImage() {
        val pickIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        pickIntent.type = "image/*"
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"
        val chooserIntent = Intent.createChooser(getIntent, "Select Image")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(chooserIntent, PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE) {
            if (data != null && data.data != null) {
                val stream: InputStream? = contentResolver.openInputStream(data.data!!)
                val bytes: ByteArray = stream!!.readBytes()
                viewModel.selectNewImage(bytes)
            }
        }
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        val id: Int = p0.itemId
        if (id == R.id.nav_exit) {
            ProjectPreferences.setToken(this, null)
            finish()
        }else{
            findNavController(R.id.nav_host_fragment).navigate(id)
        }
        return true
    }
}