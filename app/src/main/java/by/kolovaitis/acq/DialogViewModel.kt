package by.kolovaitis.acq

import android.app.Application
import android.content.BroadcastReceiver
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.ResponseHandler
import com.android.volley.VolleyError
import org.json.JSONObject

class DialogViewModel(application: Application) : AndroidViewModel(application) {
    private var refresh = true
    private val refreshThread = Thread {
        while (refresh) {
            Thread.sleep(1000)
            refreshMessages()
        }
    }
    private val httpProcessor: HttpProcessor =
        HttpProcessor(getApplication<Application>().applicationContext)

    private var _login: String? = ""
    var login: String?
        get() = _login
        set(login: String?) {
            _login = login
            refreshMessages()
        }

    fun start() {
        refreshThread.start()
    }

    fun stop() {
        refresh = false
    }

    private val messages: MutableList<Message> = mutableListOf()
    private val _messagesLive = MutableLiveData<List<Message>>().apply { messages }
    val messagesLive: LiveData<List<Message>> = _messagesLive

    fun refreshMessages() {
        httpProcessor.messagesWith(_login!!, RefreshedMessagesHandler())
    }

    fun sendMessage(text: String) {
        httpProcessor.messageTo(_login!!, text, RefreshedMessagesHandler())
    }

    inner class RefreshedMessagesHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            val newMessagesJSON = JSONObject(response as String).getJSONArray("messages")
            var wasNew = false
            for (i in 0 until newMessagesJSON.length()) {
                val messageJSON = newMessagesJSON.get(i) as JSONObject
                if (messages.find { it.id == messageJSON.getInt("id") } == null) {
                    wasNew = true
                    messages.add(
                        Message(
                            messageJSON.getString("from"),
                            messageJSON.getString("to"),
                            messageJSON.getString("text"),
                            messageJSON.getInt("id")
                        )
                    )
                }
            }
            if (wasNew) {
                _messagesLive.postValue(messages.sortedBy { it.id })
            }
        }

        override fun responseError(error: VolleyError) {
        }

    }
}

data class Message(
    var sender: String? = null,
    var receiver: String? = null,
    var text: String? = null,
    var id: Int? = null
)
