package by.kolovaitis.acq

import android.app.Application
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.ResponseHandler
import com.android.volley.VolleyError
import com.yandex.mapkit.geometry.Point
import org.json.JSONObject


class MyViewModel(application: Application) : AndroidViewModel(application) {

    private val httpProcessor: HttpProcessor =
        HttpProcessor(getApplication<Application>().applicationContext)

    private val localMe by lazy {
        User().apply {
            httpProcessor.getMyCredentials(MyCredentialsHandler())
            httpProcessor.getMyImage(MyImageHandler())
        }
    }
    private val _me = MutableLiveData<User>().apply {
        value = localMe
    }

    private val _usersForMe: MutableLiveData<List<User>> by lazy {
        MutableLiveData<List<User>>().also { loadUsersForMeFirst() }
    }


    val usersForMe: LiveData<List<User>> = _usersForMe

    var login: String?
        get() = localMe.login
        set(login: String?) {
            localMe.login = login
            _me.postValue(localMe)
        }

    var name: String?
        get() = localMe.name
        set(name: String?) {
            localMe.name = name
            _me.postValue(localMe)
        }

    var image: Bitmap?
        get() = localMe.image
        set(bitmap: Bitmap?) {
            localMe.image = bitmap
            _me.postValue(localMe)
        }

    val me: LiveData<User> = _me

    private var _myLocation = Location()

    var myLocation: Location
        get() = _myLocation
        set(location: Location) {
            httpProcessor.setLocation(location, null)
            _myLocation = location
            _myLocationLive.postValue(location)
        }
    private val _myLocationLive = MutableLiveData<Location>().apply {
        value = myLocation
    }
    val myLocationLive: LiveData<Location> = _myLocationLive
    fun selectNewImage(bytes: ByteArray) {
        httpProcessor.setImage(bytes, MyNewImageHandler())
    }

    fun loadUsersForMe() {
        _usersForMe.postValue(listOf())
        httpProcessor.getUsersForMe(UsersForMeHandler())
    }

    private fun loadUsersForMeFirst() {
        httpProcessor.getUsersForMe(UsersForMeHandler())
    }

    inner class UsersForMeHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            val users = JSONObject(response as String).getJSONArray("users")
            for (i in 0 until users.length()) {
                httpProcessor.getLoginCredentials(users.getString(i), AddUserToList(_usersForMe))
            }
        }

        override fun responseError(error: VolleyError) {
        }

    }

    inner class AddUserToList(
        val list: MutableLiveData<List<User>>,
        val needImage: Boolean = false
    ) : ResponseHandler {
        override fun responseOk(response: Any) {
            val jsonUser = JSONObject(response as String).getJSONObject("credentials")
            val user = User(
                name = jsonUser.getString("name"),
                login = jsonUser.getString("login"),
                location = Location(
                    jsonUser.getString("cord1").toDouble(),
                    jsonUser.getString("cord2").toDouble()
                )
            )
            if (needImage) {
                httpProcessor.getLoginImage(user.login!!,UserImageHandler(user, list))
            }
            val newList = list.value?.toMutableList() ?: mutableListOf()
            newList?.add(user)
            list.postValue(newList)
        }

        override fun responseError(error: VolleyError) {
        }
    }

    inner class UserImageHandler(val user: User, val liveData: MutableLiveData<List<User>>) :
        ResponseHandler {
        override fun responseOk(response: Any) {
            user.image = response as Bitmap
            liveData.postValue(liveData.value)
        }

        override fun responseError(error: VolleyError) {
        }

    }

    inner class MyNewImageHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            httpProcessor.getMyImage(MyImageHandler())
        }

        override fun responseError(error: VolleyError) {
        }
    }

    inner class MyCredentialsHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            val credentials = JSONObject(response as String).getJSONObject("credentials")
            name = credentials.getString("name")
            login = credentials.getString("login")
        }

        override fun responseError(error: VolleyError) {
        }
    }

    inner class MyImageHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            Log.d("image", "handled")
            image = response as Bitmap
        }

        override fun responseError(error: VolleyError) {
        }
    }


    private val _myPairsLive = MutableLiveData<List<User>>().apply { value = mutableListOf() }
    val myPairsLive: LiveData<List<User>> = _myPairsLive

    fun refreshMyPairs() {
        httpProcessor.refreshMyPairs(PairsHandler())
    }

    inner class PairsHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            _myPairsLive.postValue(emptyList())
            val users = JSONObject(response as String).getJSONArray("pairs")
            for (i in 0 until users.length()) {
                httpProcessor.getLoginCredentials(users.getString(i), AddUserToList(_myPairsLive, true))
            }
        }

        override fun responseError(error: VolleyError) {
        }

    }
}


data class Location(val latitude: Double? = null, val altitude: Double? = null) {
    fun toPoint(): Point {
        return Point(latitude ?: 0.0, altitude ?: 0.0)
    }
}

data class User(
    var name: String? = null,
    var login: String? = null,
    var image: Bitmap? = null,
    var location: Location? = null
)