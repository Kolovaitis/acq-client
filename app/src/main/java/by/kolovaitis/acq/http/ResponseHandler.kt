package by.kolovaitis.acq.http

import com.android.volley.VolleyError

interface ResponseHandler {
    fun responseOk(response: Any)
    fun responseError(error: VolleyError)
}
