package by.kolovaitis.acq.http

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import by.kolovaitis.acq.Location
import by.kolovaitis.acq.ProjectPreferences
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


class HttpProcessor(val context: Context) {
    companion object {
        const val HOST_URL = "http://rocky-ravine-55399.herokuapp.com"
        const val CONTENT_TYPE = "Content-Type"
        const val JSON_TYPE = "application/json"
        const val IMAGE_TYPE = "image/png"
        const val AUTHORIZATION = "Authorization"
        const val BEARER = "Bearer"
    }

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(context.applicationContext)
    }

    fun login(login: String, password: String, handler: ResponseHandler) {
        val url = "$HOST_URL/login"
        val loginRequest: StringRequest =
            object : StringRequest(
                Method.POST, url,
                Response.Listener { response ->
                    ProjectPreferences.setToken(context, JSONObject(response).getString("token"))
                    handler.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler.responseError(error)
                }
            ) {
                override fun getBody(): ByteArray {
                    val jsonObject = JSONObject()
                    jsonObject.put("login", login)
                    jsonObject.put("password", password)
                    return jsonObject.toString().toByteArray()
                }

                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(CONTENT_TYPE to JSON_TYPE)
                }
            }

        requestQueue.add(loginRequest)
    }

    fun register(login: String, password: String, name: String, handler: ResponseHandler) {
        val url = "$HOST_URL/register"
        val registerRequest: StringRequest =
            object : StringRequest(
                Method.POST, url,
                Response.Listener { response ->
                    ProjectPreferences.setToken(context, JSONObject(response).getString("token"))
                    handler.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler.responseError(error)
                }
            ) {
                override fun getBody(): ByteArray {
                    val jsonObject = JSONObject()
                    jsonObject.put("login", login)
                    jsonObject.put("password", password)
                    jsonObject.put("name", name)
                    return jsonObject.toString().toByteArray()
                }

                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(CONTENT_TYPE to JSON_TYPE)
                }
            }

        requestQueue.add(registerRequest)
    }

    fun setLocation(location: Location, handler: ResponseHandler?) {
        val url = "$HOST_URL/location"
        val locationRequest: StringRequest =
            object : StringRequest(
                Method.POST, url,
                Response.Listener { response ->
                    handler?.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler?.responseError(error)
                }
            ) {
                override fun getBody(): ByteArray {
                    val jsonObject = JSONObject()
                    jsonObject.put("first", location.latitude.toString())
                    jsonObject.put("second", location.altitude.toString())
                    return jsonObject.toString().toByteArray()
                }

                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(
                        CONTENT_TYPE to JSON_TYPE,
                        AUTHORIZATION to "$BEARER ${ProjectPreferences.getToken(context)}"
                    )
                }
            }

        requestQueue.add(locationRequest)
    }

    fun setImage(bytes: ByteArray, handler: ResponseHandler) {
        val url = "$HOST_URL/image"
        val imageRequest: StringRequest =
            object : StringRequest(
                Method.POST, url,
                Response.Listener { response ->
                    handler.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler.responseError(error)
                }
            ) {
                override fun getBody(): ByteArray {
                    return bytes
                }

                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(
                        CONTENT_TYPE to "$IMAGE_TYPE",
                        AUTHORIZATION to "$BEARER ${ProjectPreferences.getToken(context)}"
                    )
                }
            }

        requestQueue.add(imageRequest)
    }

    fun getMyCredentials(handler: ResponseHandler) {
        val url = "$HOST_URL/credentials"
        makeGetRequest(url, handler)
    }

    fun getMyImage(handler: ResponseHandler) {
        val url = "$HOST_URL/image"
        getImage(url, handler)
    }

    fun getLoginImage(login: String, handler: ResponseHandler) {
        val url = "$HOST_URL/image/$login"
        getImage(url, handler)
    }

    private fun getImage(url: String, handler: ResponseHandler) {
        val imageRequest: ImageRequest =
            object : ImageRequest(
                url,
                Response.Listener<Bitmap> { response ->
                    handler.responseOk(response)
                }, 0, 0, null,
                Response.ErrorListener { error ->
                    error.printStackTrace()
                    handler.responseError(error)
                }
            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(
                        CONTENT_TYPE to JSON_TYPE,
                        AUTHORIZATION to "$BEARER ${ProjectPreferences.getToken(context)}"
                    )
                }
            }

        requestQueue.add(imageRequest)
    }

    fun getUsersForMe(handler: ResponseHandler) {
        val url = "$HOST_URL/usersFor"
        makeGetRequest(url, handler)
    }

    private fun makeGetRequest(url: String, handler: ResponseHandler) {
        val request: StringRequest =
            object : StringRequest(
                Method.GET, url,
                Response.Listener { response ->
                    handler.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler.responseError(error)
                }
            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(
                        CONTENT_TYPE to JSON_TYPE,
                        AUTHORIZATION to "$BEARER ${ProjectPreferences.getToken(context)}"
                    )
                }
            }

        requestQueue.add(request)
    }

    fun getLoginCredentials(login: String, handler: ResponseHandler) {
        val url = "$HOST_URL/credentials/$login"
        makeGetRequest(url, handler)
    }

    fun like(login: String, handler: ResponseHandler) {
        val url = "$HOST_URL/like/$login"
        sendEmptyPost(url, handler)
    }

    fun watch(login: String, handler: ResponseHandler) {
        val url = "$HOST_URL/watch/$login"
        sendEmptyPost(url, handler)
    }

    fun refreshMyPairs(handler: ResponseHandler) {
        val url = "$HOST_URL/pairs"
        makeGetRequest(url, handler)
    }

    fun messagesWith(login: String, handler: ResponseHandler) {
        val url = "$HOST_URL/messages/$login"
        makeGetRequest(url, handler)
    }

    fun messageTo(login: String, message: String, handler: ResponseHandler) {
        val url = "$HOST_URL/message/$login"
        val messageRequest: StringRequest =
            object : StringRequest(
                Method.POST, url,
                Response.Listener { response ->
                    handler.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler.responseError(error)
                }
            ) {
                override fun getBody(): ByteArray {
                    val jsonObject = JSONObject()
                    jsonObject.put("text", message)
                    return jsonObject.toString().toByteArray()
                }

                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(
                        CONTENT_TYPE to JSON_TYPE,
                        AUTHORIZATION to "$BEARER ${ProjectPreferences.getToken(context)}"
                    )
                }
            }
        requestQueue.add(messageRequest)
    }

    private fun sendEmptyPost(url: String, handler: ResponseHandler?) {
        val locationRequest: StringRequest =
            object : StringRequest(
                Method.POST, url,
                Response.Listener { response ->
                    handler?.responseOk(response)
                },
                Response.ErrorListener { error ->
                    handler?.responseError(error)
                }
            ) {
                override fun getBody(): ByteArray {
                    return byteArrayOf()
                }

                override fun getHeaders(): MutableMap<String, String> {
                    return mutableMapOf(
                        CONTENT_TYPE to JSON_TYPE,
                        AUTHORIZATION to "$BEARER ${ProjectPreferences.getToken(context)}"
                    )
                }
            }

        requestQueue.add(locationRequest)
    }


}