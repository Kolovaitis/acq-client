package by.kolovaitis.acq.http

enum class Status {
    OK, UNKNOWN, ERROR, NOT_STARTED
}