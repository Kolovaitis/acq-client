package by.kolovaitis.acq.ui.profile

import android.app.Application
import android.graphics.Bitmap
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import by.kolovaitis.acq.User
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.ResponseHandler
import com.android.volley.VolleyError
import org.json.JSONObject

class ProfileViewModel(application: Application) : AndroidViewModel(application) {
    val httpProcessor: HttpProcessor =
        HttpProcessor(getApplication<Application>().applicationContext)
    private val _user = MutableLiveData<User>()
    val user: LiveData<User> = _user

    private val _wasLiked = MutableLiveData<Boolean>().apply { value = false }
    val wasLiked: LiveData<Boolean> = _wasLiked

    fun setLogin(login: String) {
        httpProcessor.getLoginCredentials(login, GetLoginCredentialsHandler())
    }

    fun like() {
        httpProcessor.like(user.value?.login ?: "", LikeHandler())
    }

    fun dislike() {
        httpProcessor.watch(user.value?.login ?: "", LikeHandler())
    }

    inner class LikeHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            _wasLiked.postValue(true)
        }

        override fun responseError(error: VolleyError) {
        }

    }

    inner class GetLoginCredentialsHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            val jsonObject = JSONObject(response as String).getJSONObject("credentials")
            val user =
                User(name = jsonObject.getString("name"), login = jsonObject.getString("login"))
            httpProcessor.getLoginImage(user.login!!, GetLoginImageHandler())
            _user.postValue(user)
        }

        override fun responseError(error: VolleyError) {
        }

    }

    inner class GetLoginImageHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            val user = _user.value
            user?.image = response as Bitmap
            _user.postValue(user)
        }

        override fun responseError(error: VolleyError) {
        }

    }

}