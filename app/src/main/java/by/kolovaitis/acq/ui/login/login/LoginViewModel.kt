package by.kolovaitis.acq.ui.login.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.ResponseHandler
import by.kolovaitis.acq.http.Status
import com.android.volley.VolleyError

class LoginViewModel : ViewModel() {
    private lateinit var httpProcessor: HttpProcessor


    private val _loginStatus = MutableLiveData<Status>().apply {
        value = Status.NOT_STARTED
    }
    val loginStatus: LiveData<Status> = _loginStatus

    fun login(login: String, password: String) {
        httpProcessor.login(login, password, LoginResponseHandler())
        _loginStatus.postValue(Status.UNKNOWN)
    }

    fun setHttpProcessor(httpProcessor: HttpProcessor) {
        this.httpProcessor = httpProcessor
    }

    inner class LoginResponseHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            _loginStatus.postValue(Status.OK)
        }

        override fun responseError(error: VolleyError) {
            _loginStatus.postValue(Status.ERROR)
        }

    }
}