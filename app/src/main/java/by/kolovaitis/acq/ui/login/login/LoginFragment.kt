package by.kolovaitis.acq.ui.login.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import by.kolovaitis.acq.MainActivity
import by.kolovaitis.acq.R
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.Status

class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loginViewModel =
            ViewModelProviders.of(this).get(LoginViewModel::class.java)
        loginViewModel.setHttpProcessor(HttpProcessor(context!!))
        val root = inflater.inflate(R.layout.fragment_login, container, false)
        val progressBar = root.findViewById<ProgressBar>(R.id.progressBar)
        val loginView = root.findViewById<EditText>(R.id.editLogin)
        val passwordView = root.findViewById<EditText>(R.id.editPassword)
        val button = root.findViewById<Button>(R.id.buttonLogin)
        button.setOnClickListener {
            val login = loginView.text.toString()
            val password = passwordView.text.toString()
            if ((login.isNotEmpty()) && (password.isNotEmpty())) {
                loginViewModel.login(login, password)
            } else {
                Toast.makeText(context, getString(R.string.empty_fields), Toast.LENGTH_SHORT).show()
            }
        }
        loginViewModel.loginStatus.observe(viewLifecycleOwner, Observer {
            when (it) {
                Status.UNKNOWN -> progressBar.visibility = View.VISIBLE
                Status.NOT_STARTED -> progressBar.visibility = View.GONE
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(
                        context,
                        getString(R.string.connection_error),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                Status.OK -> {
                    progressBar.visibility = View.GONE
                    context!!.startActivity(Intent(context, MainActivity::class.java))
                    activity?.finish()

                }
            }
        })
        return root
    }
}