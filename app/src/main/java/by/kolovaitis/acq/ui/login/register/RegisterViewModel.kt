package by.kolovaitis.acq.ui.login.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.ResponseHandler
import by.kolovaitis.acq.http.Status
import com.android.volley.VolleyError

class RegisterViewModel : ViewModel() {
    private lateinit var httpProcessor: HttpProcessor
    private lateinit var lastError: String

    private val _registerStatus = MutableLiveData<Status>().apply {
        value = Status.NOT_STARTED
    }
    val loginStatus: LiveData<Status> = _registerStatus

    fun register(login: String, password: String, name:String) {
        httpProcessor.register(login, password,name, LoginResponseHandler())
        _registerStatus.postValue(Status.UNKNOWN)
    }

    fun setHttpProcessor(httpProcessor: HttpProcessor) {
        this.httpProcessor = httpProcessor
    }

    inner class LoginResponseHandler : ResponseHandler {
        override fun responseOk(response: Any) {
            _registerStatus.postValue(Status.OK)
        }

        override fun responseError(error: VolleyError) {
            lastError = error.message?:""
            _registerStatus.postValue(Status.ERROR)
        }

    }
    fun getLastError()=lastError
}