package by.kolovaitis.acq.ui.pairs

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import by.kolovaitis.acq.MainActivity
import by.kolovaitis.acq.MyViewModel
import by.kolovaitis.acq.R
import by.kolovaitis.acq.ui.profile.ProfileViewModel


class PairFragment : Fragment() {
    private val viewModel: MyViewModel by activityViewModels()
    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pair_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = MyPairRecyclerViewAdapter().apply {
                    viewModel.myPairsLive.observe(viewLifecycleOwner, Observer {
                        this.setValues(it)
                    })
                }
            }
        }
        return view
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            PairFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
    override fun onStart() {
        super.onStart()
        (activity as MainActivity).fab?.setOnClickListener {
            viewModel.refreshMyPairs()
        }
        viewModel.refreshMyPairs()
    }
}