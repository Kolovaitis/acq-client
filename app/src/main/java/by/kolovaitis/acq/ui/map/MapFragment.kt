package by.kolovaitis.acq.ui.map

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import by.kolovaitis.acq.MainActivity
import by.kolovaitis.acq.MyViewModel
import by.kolovaitis.acq.ProfileActivity
import by.kolovaitis.acq.R
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.PlacemarkMapObject
import com.yandex.mapkit.mapview.MapView
import com.yandex.runtime.ui_view.ViewProvider


class MapFragment : Fragment() {
    lateinit var mapview: MapView
    private val viewModel: MyViewModel by activityViewModels()
    private var meOnMap: PlacemarkMapObject? = null
    val otherUsersPositions = mutableListOf<PlacemarkMapObject>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        mapview = view.findViewById<View>(R.id.mapview) as MapView
        viewModel.myLocationLive.observe(viewLifecycleOwner, Observer {
            mapview.map.move(
                CameraPosition(it.toPoint(), 11.0f, 0.0f, 0.0f),
                Animation(Animation.Type.SMOOTH, 0f),
                null
            )
            if (meOnMap != null) {
                mapview.map.mapObjects.remove(meOnMap!!)
            }
            val meView = LayoutInflater.from(context)
                .inflate(
                    R.layout.me_view,
                    null
                )
            meOnMap = mapview.map.mapObjects.addPlacemark(it.toPoint(), ViewProvider(meView))
        })
        viewModel.usersForMe.observe(viewLifecycleOwner, Observer { list ->
            if (list != null) {
                otherUsersPositions.forEach {
                    mapview.map.mapObjects.remove(it)
                }
                otherUsersPositions.clear()
                list.forEach {
                    val otherView = LayoutInflater.from(context)
                        .inflate(
                            R.layout.others_view,
                            null
                        )

                    otherUsersPositions.add(
                        mapview.map.mapObjects.addPlacemark(
                            it.location!!.toPoint(),
                            ViewProvider(otherView)
                        ).apply {
                            this.addTapListener { mapObject, point ->
                                val intent = Intent(context, ProfileActivity::class.java)
                                intent.putExtra("login", it.login)
                                context?.startActivity(intent)
                                true
                            }
                        }
                    )
                }
            }
        })


    }


    override fun onStart() {
        super.onStart()
        mapview.onStart()
        MapKitFactory.getInstance().onStart()
        (activity as MainActivity).fab?.setOnClickListener {
            viewModel.loadUsersForMe()
        }
        viewModel.loadUsersForMe()
    }

    override fun onStop() {
        super.onStop()
        mapview.onStop();
        MapKitFactory.getInstance().onStop();
    }


}