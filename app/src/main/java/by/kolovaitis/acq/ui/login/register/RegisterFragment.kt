package by.kolovaitis.acq.ui.login.register

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import by.kolovaitis.acq.MainActivity
import by.kolovaitis.acq.R
import by.kolovaitis.acq.http.HttpProcessor
import by.kolovaitis.acq.http.Status

class RegisterFragment : Fragment() {

    private lateinit var registerViewModel: RegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        registerViewModel =
            ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        registerViewModel.setHttpProcessor(HttpProcessor(context!!))

        val root = inflater.inflate(R.layout.fragment_register, container, false)
        val progressBar = root.findViewById<ProgressBar>(R.id.progressBar)

        val loginView = root.findViewById<EditText>(R.id.editLogin)
        val passwordView = root.findViewById<EditText>(R.id.editPassword)
        val nameView = root.findViewById<EditText>(R.id.editName)

        val button = root.findViewById<Button>(R.id.buttonRegister)
        button.setOnClickListener {
            val login = loginView.text.toString()
            val password = passwordView.text.toString()
            val name = nameView.text.toString()

            if ((login.isNotEmpty()) && (password.isNotEmpty())&&(name.isNotEmpty())) {
                registerViewModel.register(login, password, name)
            } else {
                Toast.makeText(context, getString(R.string.empty_fields), Toast.LENGTH_SHORT).show()
            }
        }
        registerViewModel.loginStatus.observe(viewLifecycleOwner, Observer {
            when (it) {
                Status.UNKNOWN -> progressBar.visibility = View.VISIBLE
                Status.NOT_STARTED -> progressBar.visibility = View.GONE
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    Toast.makeText(
                        context,
                        "${getString(R.string.connection_error)}\n${registerViewModel.getLastError()}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                Status.OK -> {
                    progressBar.visibility = View.GONE
                    context!!.startActivity(Intent(context, MainActivity::class.java))
                    activity?.finish()
                }
            }
        })
        return root
    }
}