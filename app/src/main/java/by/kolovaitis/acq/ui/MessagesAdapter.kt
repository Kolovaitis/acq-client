package by.kolovaitis.acq.ui

import android.os.Build
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.acq.Message
import by.kolovaitis.acq.R
import by.kolovaitis.acq.ui.pairs.MyPairRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_message.view.*

class MessagesAdapter(val opponentLogin: String) : RecyclerView.Adapter<MessagesAdapter.ViewHolder>() {
    private var _values = mutableListOf<Message>()
    var values: List<Message>
        get() = _values
        set(newValues: List<Message>) {
            _values = newValues.toMutableList()
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_message, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return _values.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.mText.text = item.text



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (item.sender.equals(opponentLogin)) {//message from opponent
                holder.mCloud.setCardBackgroundColor(holder.mView.context.getColor(R.color.colorPrimary))
            } else {//message from me
                holder.mCloud.setCardBackgroundColor(holder.mView.context.getColor(R.color.colorAccent))
            }
        }
        if (item.sender.equals(opponentLogin)) {
           holder.mLinear.gravity = Gravity.LEFT
        }else{
            holder.mLinear.gravity = Gravity.RIGHT

        }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mLinear:LinearLayout = mView.linear
        val mCloud: CardView = mView.cloud
        val mText: TextView = mView.text
    }
}