package by.kolovaitis.acq.ui.pairs

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import by.kolovaitis.acq.DialogActivity
import by.kolovaitis.acq.R
import by.kolovaitis.acq.User


class MyPairRecyclerViewAdapter(
) : RecyclerView.Adapter<MyPairRecyclerViewAdapter.ViewHolder>() {
    private var values: MutableList<User> = mutableListOf<User>()

    fun setValues(newValues: List<User>) {
        values = newValues.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_pair, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.contentView.text = item.name ?: ""
        if (item.image != null) {
            holder.imageView.setImageBitmap(item.image)
        } else {
            holder.imageView.setImageResource(R.drawable.ic_twotone_face_24)
        }

        holder.view.setOnClickListener {
            val intent = Intent(it.context, DialogActivity::class.java)
            intent.putExtra("name", item.name)
            intent.putExtra("login", item.login)
            it.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.imageView)
        val contentView: TextView = view.findViewById(R.id.content)
        val view = view
        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }
}