package by.kolovaitis.acq.ui.profile

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import by.kolovaitis.acq.R
import kotlinx.android.synthetic.main.profile_fragment.view.*

class ProfileFragment(val login: String) : Fragment() {

    private val viewModel: ProfileViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.setLogin(login)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.user.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it.image != null) {
                    view.imageView.setImageBitmap(it.image)
                }
                if (it.name != null) {
                    view.textViewName.text = it.name
                }
            }
        })
        viewModel.wasLiked.observe(viewLifecycleOwner, Observer {
            if(it){
                activity?.finish()
            }
        })
        view.buttonLike.setOnClickListener {
            viewModel.like()
        }
        view.buttonNotLike.setOnClickListener {
            viewModel.dislike()
        }
    }

}