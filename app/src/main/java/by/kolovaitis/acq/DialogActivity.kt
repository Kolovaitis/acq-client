package by.kolovaitis.acq

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import by.kolovaitis.acq.ui.MessagesAdapter
import kotlinx.android.synthetic.main.activity_dialog.*

class DialogActivity : AppCompatActivity() {

    val viewModel: DialogViewModel by viewModels()

    var name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        name = intent.getStringExtra("name")
        supportActionBar?.title = name

        val login = intent.getStringExtra("login")
        viewModel.login = login

        with(recycler_view) {
            layoutManager =
                LinearLayoutManager(this@DialogActivity, LinearLayoutManager.VERTICAL, false)
            adapter = MessagesAdapter(
                login
            ).apply {
                viewModel.messagesLive.observe(this@DialogActivity, Observer {
                    this.values = it
                    smoothScrollToPosition(it.size - 1)
                })
            }
        }

        val buttonSend = imageButton
        val messageText = messageText

        buttonSend.setOnClickListener {
            val text = messageText.text.toString()
            if (text.isNotEmpty()) {
                viewModel.sendMessage(text)
                messageText.text.clear()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.start()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stop()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}